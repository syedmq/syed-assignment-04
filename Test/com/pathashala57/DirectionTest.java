package com.pathashala57;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.testng.AssertJUnit.assertEquals;

class DirectionTest {

    @Test
    void eastEqualsEastDirection(){
        assertEquals(Direction.EAST, Direction.EAST);
    }
    @Test
    void turnsLeftwhenTurnedLeft(){
        assertEquals(Direction.WEST, Direction.NORTH.rotateLeft());
    }
    @Test
    void turnsRightwhenTurnedRight(){
        assertEquals(Direction.NORTH, Direction.WEST.rotateRight());
    }
}