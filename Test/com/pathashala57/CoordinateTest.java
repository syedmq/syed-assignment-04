package com.pathashala57;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CoordinateTest {
    @Test
    void returnsFalseWhenComparedToNull() {
        Coordinate aCoordinate=new Coordinate(1,1);
        assertFalse(aCoordinate.equals(null));
    }

    @Test
    void returnsTrueWhenComparedToitself() {
        Coordinate aCoordinate=new Coordinate(1,1);
        assertTrue(aCoordinate.equals(aCoordinate));
    }

    @Test
    void returnsFalseWhenComparedToObjectFromDifferentClass() {
        Coordinate aCoordinate=new Coordinate(1,1);
        assertFalse(aCoordinate.equals(new Object()));
    }

    @Test
    void returnsTrueWhenComparedToEquivalentCoordinate() {
        Coordinate aCoordinate=new Coordinate(1,1);
        Coordinate anotherCoordinate=new Coordinate(1,1);
        assertEquals(aCoordinate,anotherCoordinate);
    }


}