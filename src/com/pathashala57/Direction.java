package com.pathashala57;

//Represents alignment of Rover

public enum  Direction {

    NORTH {
        @Override
        Direction rotateRight() {
            return Direction.EAST;
        }

        @Override
        Direction rotateLeft() {
            return Direction.WEST;
        }

    },

    WEST{
        @Override
        Direction rotateRight() {
            return Direction.NORTH;
        }

        @Override
        Direction rotateLeft() {
            return Direction.SOUTH;
        }

    },

    EAST {
        @Override
        Direction rotateRight() {
            return Direction.SOUTH;
        }

        @Override
        Direction rotateLeft() {
            return Direction.EAST;
        }
    },

    SOUTH{
        @Override
        Direction rotateRight() {
            return Direction.WEST;
        }

        @Override
        Direction rotateLeft() {
            return Direction.EAST;
        }
    };
    Direction() {

    }

    abstract Direction rotateRight();
    abstract Direction rotateLeft();
}
