package com.pathashala57;

//Represents a point on the plateu

public class Coordinate {
    private final int xCoordinate;
    private final int yCoordinate;

    Coordinate(int xCoordinate, int yCoordinate) {
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
    }

    @Override
    public boolean equals(Object that) {
        if(that==null)
            return false;
        if(that==this)
            return true;
        if(that.getClass()!=this.getClass())
            return false;
        Coordinate anotherCoordinate=(Coordinate)that;
        return anotherCoordinate.xCoordinate == xCoordinate && anotherCoordinate.yCoordinate == yCoordinate;
    }
}
